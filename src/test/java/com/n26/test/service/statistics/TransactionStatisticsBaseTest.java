package com.n26.test.service.statistics;

import com.n26.test.model.TransactionStatistics;
import org.joda.time.DateTimeUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public abstract class TransactionStatisticsBaseTest {
    protected static final long MINUTE_MS = TimeUnit.MINUTES.toMillis(1);
    protected static final double DELTA = 0.001;

    @Before
    public void setup() {
        DateTimeUtils.setCurrentMillisFixed(0);
    }

    @Test
    public void consistent_whenMultipleTransactionAtTime() {
        postTransaction(10, 0);
        postTransaction(20, 0);

        TransactionStatistics stat = getStatistics();
        assertEquals(2, stat.getCount());
        assertEquals(30, stat.getSum(), DELTA);
        assertEquals(15, stat.getAvg(), DELTA);
        assertEquals(20, stat.getMax(), DELTA);
        assertEquals(10, stat.getMin(), DELTA);
    }

    @Test
    public void consistent_whenOnlyOneTransaction() {
        postTransaction(10, 0);
        assertEquals(1, getStatistics().getCount());
    }

    @Test
    public void consistent_whenNoTransactions() {
        TransactionStatistics stats = getStatistics();
        assertEquals(0, stats.getSum(), DELTA);
        assertEquals(0, stats.getCount());
        assertEquals(0, stats.getMin(), DELTA);
        assertEquals(0, stats.getMax(), DELTA);
        assertEquals(0, stats.getAvg(), DELTA);
    }

    @Test
    public void consistent_whenTransactionFromPast() {
        postTransaction(10, 0);
        DateTimeUtils.setCurrentMillisFixed(MINUTE_MS);
        assertEquals(10, getStatistics().getSum(), DELTA);

        postTransaction(10, MINUTE_MS - 1);
        assertEquals(20, getStatistics().getSum(), DELTA);
    }

    @Test
    public void consistent_whenManyThreadsPostingInWindow() throws InterruptedException {
        // post transactions to 0..MINUTE_MS window, massive concurrent write to collection
        IntSummaryStatistics sample = postMultiThreadInWindow(30, 100000);

        DateTimeUtils.setCurrentMillisFixed(MINUTE_MS);
        TransactionStatistics evaluated = getStatistics();
        assertEquals(sample.getCount(), evaluated.getCount());
        assertEquals(sample.getAverage(), evaluated.getAvg(), DELTA);
        assertEquals(sample.getMax(), evaluated.getMax(), DELTA);
        assertEquals(sample.getMin(), evaluated.getMin(), DELTA);
        assertEquals(sample.getSum(), evaluated.getSum(), DELTA);
    }

    @Test
    public void consistent_whenManyThreadsWantsCleanedStat() throws InterruptedException {
        int threadCount = 30;
        // post transactions to 0..MINUTE_MS window
        postMultiThreadInWindow(threadCount, 100000);

        // get stat for 2*MINUTE_MS..3*MINUTE_MS what leads to massive concurrent deletion from collection
        // all fetched stats should be 0
        AtomicBoolean allClean = new AtomicBoolean(true);
        List<Thread> threads = IntStream.range(0, threadCount).mapToObj(threadIndex -> new Thread(() -> {
            DateTimeUtils.setCurrentMillisFixed(2 * MINUTE_MS + 30);
            try {
                TransactionStatistics statistics = getStatistics();
                if (statistics.getCount() != 0) allClean.set(false);
                if (statistics.getAvg() != 0) allClean.set(false);
                if (statistics.getSum() != 0) allClean.set(false);
                if (statistics.getMax() != 0) allClean.set(false);
                if (statistics.getMin() != 0) allClean.set(false);
            } catch (Exception e) {
                allClean.set(false);
            }
        })).map(thread -> {
            thread.start();
            return thread;
        }).collect(toList());
        for (Thread thread : threads) {
            thread.join();
        }
        assertTrue(allClean.get());
    }

    private IntSummaryStatistics postMultiThreadInWindow(int threadCount, int transactionsPerThread) throws InterruptedException {
        ConcurrentLinkedQueue<IntSummaryStatistics> allStatistics = new ConcurrentLinkedQueue<>();
        List<Thread> threads = IntStream.range(0, threadCount).mapToObj(threadIndex -> new Thread(() -> {
            DateTimeUtils.setCurrentMillisFixed(MINUTE_MS);
            IntSummaryStatistics stats = IntStream.range(1, transactionsPerThread).map(amount -> {
                postTransaction(amount, ThreadLocalRandom.current().nextInt((int) MINUTE_MS));
                return amount;
            }).summaryStatistics();
            allStatistics.add(stats);
        })).map(thread -> {
            thread.start();
            return thread;
        }).collect(toList());
        for (Thread thread : threads) {
            thread.join();
        }
        IntSummaryStatistics result = allStatistics.poll();
        for (IntSummaryStatistics statistics : allStatistics) {
            result.combine(statistics);
        }
        return result;
    }

    protected abstract TransactionStatistics getStatistics();

    protected abstract void postTransaction(double amount, long timestamp);
}
