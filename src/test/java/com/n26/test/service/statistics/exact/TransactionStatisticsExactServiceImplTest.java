package com.n26.test.service.statistics.exact;

import com.n26.test.model.Transaction;
import com.n26.test.model.TransactionStatistics;
import com.n26.test.service.statistics.TransactionStatisticsBaseTest;
import com.n26.test.service.transaction.event.TransactionProcessedEvent;
import org.joda.time.DateTimeUtils;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TransactionStatisticsExactServiceImplTest extends TransactionStatisticsBaseTest {

    private TransactionStatisticsExactServiceImpl service = new TransactionStatisticsExactServiceImpl();

    @Test
    public void consistent_whenTransactionOutOfWindow() {
        DateTimeUtils.setCurrentMillisFixed(MINUTE_MS + 1);
        postTransaction(10, 0);
        postTransaction(20, MINUTE_MS);

        TransactionStatistics stat = getStatistics();
        assertEquals(1, stat.getCount());
        assertEquals(20, stat.getMin(), DELTA);
        assertEquals(20, stat.getSum(), DELTA);
    }

    @Override
    protected TransactionStatistics getStatistics() {
        return service.getLastMinuteStatistics();
    }

    @Override
    protected void postTransaction(double amount, long timestamp) {
        service.onTransactionProcessed(new TransactionProcessedEvent(new Transaction(amount, timestamp)));
    }
}