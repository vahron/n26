package com.n26.test.service.statistics.timeslot;

import com.n26.test.model.Transaction;
import com.n26.test.model.TransactionStatistics;
import com.n26.test.service.statistics.TransactionStatisticsBaseTest;
import com.n26.test.service.transaction.event.TransactionProcessedEvent;
import org.joda.time.DateTimeUtils;
import org.junit.Test;

import static com.n26.test.service.statistics.timeslot.TransactionStatisticsTimeSlotServiceImpl.TIMESTAMP_DIVISOR;
import static org.junit.Assert.assertEquals;

public class TransactionStatisticsTimeSlotServiceImplTest extends TransactionStatisticsBaseTest {

    private TransactionStatisticsTimeSlotServiceImpl service = new TransactionStatisticsTimeSlotServiceImpl();

    @Test
    public void consistent_whenTransactionOutOfSlot() {
        DateTimeUtils.setCurrentMillisFixed(MINUTE_MS + TIMESTAMP_DIVISOR + 1);
        postTransaction(10, 0);
        postTransaction(20, MINUTE_MS);

        TransactionStatistics stat = getStatistics();
        assertEquals(1, stat.getCount());
        assertEquals(20, stat.getMin(), DELTA);
        assertEquals(20, stat.getSum(), DELTA);
    }

    @Override
    protected TransactionStatistics getStatistics() {
        return service.getLastMinuteStatistics();
    }

    @Override
    protected void postTransaction(double amount, long timestamp) {
        service.onTransactionProcessed(new TransactionProcessedEvent(new Transaction(amount, timestamp)));
    }
}