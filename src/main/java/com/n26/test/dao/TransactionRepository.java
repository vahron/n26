package com.n26.test.dao;

import com.n26.test.model.Transaction;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface TransactionRepository extends Repository<Transaction, Long> {
    Transaction save(Transaction transaction);

    List<Transaction> findAll();

    List<Transaction> findByTimestampGreaterThanEqual(long timestamp);
}
