package com.n26.test.model;

public class NamedTransactionStatistics extends TransactionStatistics {
    private final String provider;

    public NamedTransactionStatistics(String provider, TransactionStatistics statistics) {
        super(statistics.getSum(), statistics.getAvg(), statistics.getMax(), statistics.getMin(), statistics.getCount());
        this.provider = provider;
    }

    public String getProvider() {
        return provider;
    }

    @Override
    public String toString() {
        return "NamedTransactionStatistics{" +
                "provider='" + provider + '\'' +
                "} " + super.toString();
    }
}
