package com.n26.test.service.transaction;

import com.n26.test.model.Transaction;

import java.util.List;

public interface TransactionService {
    Transaction processTransaction(Transaction transaction);

    List<Transaction> getAllTransactions();
}
