package com.n26.test.service.transaction.event;

import com.n26.test.model.Transaction;

public class TransactionProcessedEvent {
    private final Transaction transaction;

    public Transaction getTransaction() {
        return transaction;
    }

    public TransactionProcessedEvent(Transaction transaction) {
        this.transaction = transaction;
    }
}
