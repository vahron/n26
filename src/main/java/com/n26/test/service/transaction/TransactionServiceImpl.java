package com.n26.test.service.transaction;

import com.n26.test.dao.TransactionRepository;
import com.n26.test.model.Transaction;
import com.n26.test.service.transaction.event.TransactionProcessedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService {
    private final TransactionRepository repository;
    private final ApplicationEventPublisher publisher;

    @Autowired
    public TransactionServiceImpl(TransactionRepository repository, ApplicationEventPublisher publisher) {
        this.repository = repository;
        this.publisher = publisher;
    }

    @Override
    public Transaction processTransaction(Transaction transaction) {
        Transaction saved = repository.save(transaction);
        publisher.publishEvent(new TransactionProcessedEvent(saved));
        return saved;
    }

    @Override
    public List<Transaction> getAllTransactions() {
        return repository.findAll();
    }
}
