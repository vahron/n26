package com.n26.test.service.statistics.timeslot;

import com.n26.test.model.Transaction;

public class TimeSlot {
    private final double sum;
    private final long count;
    private final double min;
    private final double max;

    public TimeSlot(double sum, long count, double min, double max) {
        this.sum = sum;
        this.count = count;
        this.min = min;
        this.max = max;
    }

    public TimeSlot(Transaction transaction) {
        sum = min = max = transaction.getAmount();
        count = 1;
    }

    public TimeSlot withMergedTransaction(Transaction processed) {
        double amount = processed.getAmount();
        return new TimeSlot(sum + amount, count + 1, Math.min(min, amount), Math.max(max, amount));
    }

    public double getSum() {
        return sum;
    }

    public double getMax() {
        return max;
    }

    public double getMin() {
        return min;
    }

    public double getCount() {
        return count;
    }

    @Override
    public String toString() {
        return "TimeSlot{" +
                "sum=" + sum +
                ", max=" + max +
                ", min=" + min +
                ", count=" + count +
                '}';
    }
}
