package com.n26.test.service.statistics;

import com.n26.test.model.TransactionStatistics;

public interface TransactionStatisticsService {
    TransactionStatistics getLastMinuteStatistics();
}
