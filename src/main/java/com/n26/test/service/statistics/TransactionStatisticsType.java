package com.n26.test.service.statistics;

public interface TransactionStatisticsType {
    String DB = "TransactionStatisticsDb";
    String EXACT = "TransactionStatisticsExact";
    String TIME_SLOT = "TransactionStatisticsTimeSlot";
}
