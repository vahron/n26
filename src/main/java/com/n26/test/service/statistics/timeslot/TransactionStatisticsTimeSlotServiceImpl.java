package com.n26.test.service.statistics.timeslot;

import com.n26.test.model.Transaction;
import com.n26.test.model.TransactionStatistics;
import com.n26.test.service.statistics.TransactionStatisticsService;
import com.n26.test.service.statistics.TransactionStatisticsType;
import com.n26.test.service.transaction.event.TransactionProcessedEvent;
import org.joda.time.DateTimeUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

@Service
@Qualifier(TransactionStatisticsType.TIME_SLOT)
public class TransactionStatisticsTimeSlotServiceImpl implements TransactionStatisticsService {
    private static final long MINUTE_MS = TimeUnit.MINUTES.toMillis(1);
    private static final long SLOTS_COUNT = 600;
    public static final long TIMESTAMP_DIVISOR = MINUTE_MS / SLOTS_COUNT;

    private final ConcurrentHashMap<Long, TimeSlot> slots;

    public TransactionStatisticsTimeSlotServiceImpl() {
        slots = new ConcurrentHashMap<>();
    }

    @Override
    public TransactionStatistics getLastMinuteStatistics() {
        double sum = 0;
        long count = 0;
        double min = Double.POSITIVE_INFINITY;
        double max = Double.NEGATIVE_INFINITY;

        long currentTimestamp = DateTimeUtils.currentTimeMillis();

        for (Map.Entry<Long, TimeSlot> entry : slots.entrySet()) {
            Long slotKey = entry.getKey();
            TimeSlot slot = entry.getValue();

            if (slotKeyInWindow(currentTimestamp, slotKey)) {
                sum += slot.getSum();
                count += slot.getCount();
                min = Math.min(min, slot.getMin());
                max = Math.max(max, slot.getMax());
            } else {
                slots.remove(slotKey);
            }
        }

        return new TransactionStatistics(sum,
                                         count == 0 ? 0 : sum / count,
                                         count == 0 ? 0 : min,
                                         count == 0 ? 0 : max,
                                         count);
    }

    @EventListener
    public void onTransactionProcessed(TransactionProcessedEvent event) {
        Transaction processed = event.getTransaction();
        long slotKey = getSlotKey(processed.getTimestamp());

        if (slotKeyInWindow(DateTimeUtils.currentTimeMillis(), slotKey)) {
            slots.compute(slotKey, (key, slot) ->
                    slot == null ? new TimeSlot(processed) : slot.withMergedTransaction(processed)
            );
        }
    }

    private boolean slotKeyInWindow(long currentTimestamp, long slotKey) {
        return getSlotKey(currentTimestamp) - slotKey <= getSlotKey(MINUTE_MS);
    }

    private long getSlotKey(long timestamp) {
        return timestamp / TIMESTAMP_DIVISOR;
    }
}
