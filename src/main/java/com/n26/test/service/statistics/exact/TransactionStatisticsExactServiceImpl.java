package com.n26.test.service.statistics.exact;

import com.n26.test.model.Transaction;
import com.n26.test.model.TransactionStatistics;
import com.n26.test.service.statistics.TransactionStatisticsService;
import com.n26.test.service.statistics.TransactionStatisticsType;
import com.n26.test.service.transaction.event.TransactionProcessedEvent;
import org.joda.time.DateTimeUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

@Service
@Qualifier(TransactionStatisticsType.EXACT)
public class TransactionStatisticsExactServiceImpl implements TransactionStatisticsService {
    private static final long MINUTE_MS = TimeUnit.MINUTES.toMillis(1);
    private static final int TRANSACTIONS_INITIAL_CAPACITY = 1000;

    private final ReentrantLock updateLock;

    private volatile double sum;
    private final PriorityQueue<Transaction> transactions;
    private final TreeSet<Transaction> transactionAmounts;

    public TransactionStatisticsExactServiceImpl() {
        this.updateLock = new ReentrantLock();
        this.transactions = new PriorityQueue<>(TRANSACTIONS_INITIAL_CAPACITY,
                                                Comparator.comparingLong(Transaction::getTimestamp));
        this.transactionAmounts = new TreeSet<>(Comparator.comparingDouble(Transaction::getAmount));
    }

    @Override
    public TransactionStatistics getLastMinuteStatistics() {
        updateLock.lock();
        try {
            cleanMinuteWindow();

            long count = transactions.size();
            double avg = count == 0 ? 0 : sum / count;
            double min = count == 0 ? 0 : transactionAmounts.first().getAmount();
            double max = count == 0 ? 0 : transactionAmounts.last().getAmount();
            return new TransactionStatistics(sum, avg, min, max, count);
        } finally {
            updateLock.unlock();
        }
    }

    @EventListener
    public void onTransactionProcessed(TransactionProcessedEvent event) {
        Transaction processed = event.getTransaction();

        updateLock.lock();
        try {
            if (timestampInWindow(DateTimeUtils.currentTimeMillis(), processed.getTimestamp())) {
                updateStatisticsAdded(processed);
                cleanMinuteWindow();
            }
        } finally {
            updateLock.unlock();
        }
    }

    private void cleanMinuteWindow() {
        updateLock.lock();
        try {
            long currentTimestamp = DateTimeUtils.currentTimeMillis();

            Transaction oldest = transactions.peek();
            while (oldest != null && !timestampInWindow(currentTimestamp, oldest.getTimestamp())) {
                updateStatisticsRemoved(oldest);
                oldest = transactions.peek();
            }
        } finally {
            updateLock.unlock();
        }
    }

    private boolean timestampInWindow(long currentTimestamp, long timestamp) {
        return currentTimestamp - timestamp <= MINUTE_MS;
    }

    private void updateStatisticsAdded(Transaction added) {
        sum += added.getAmount();
        transactions.add(added);
        transactionAmounts.add(added);
    }

    private void updateStatisticsRemoved(Transaction removed) {
        sum -= removed.getAmount();
        transactions.remove(removed);
        transactionAmounts.remove(removed);
    }
}
