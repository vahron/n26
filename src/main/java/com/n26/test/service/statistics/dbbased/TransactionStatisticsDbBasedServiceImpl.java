package com.n26.test.service.statistics.dbbased;

import com.n26.test.dao.TransactionRepository;
import com.n26.test.model.Transaction;
import com.n26.test.model.TransactionStatistics;
import com.n26.test.service.statistics.TransactionStatisticsService;
import com.n26.test.service.statistics.TransactionStatisticsType;
import org.joda.time.DateTimeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
@Qualifier(TransactionStatisticsType.DB)
public class TransactionStatisticsDbBasedServiceImpl implements TransactionStatisticsService {
    private static final long MINUTE_MS = TimeUnit.MINUTES.toMillis(1);

    private final TransactionRepository repository;

    @Autowired
    public TransactionStatisticsDbBasedServiceImpl(TransactionRepository repository) {
        this.repository = repository;
    }

    @Override
    public TransactionStatistics getLastMinuteStatistics() {
        List<Transaction> transactions =
                repository.findByTimestampGreaterThanEqual(DateTimeUtils.currentTimeMillis() - MINUTE_MS);

        DoubleSummaryStatistics stats = transactions.stream().mapToDouble(Transaction::getAmount).summaryStatistics();

        return new TransactionStatistics(stats.getSum(),
                                         stats.getAverage(),
                                         stats.getMin() == Double.POSITIVE_INFINITY ? 0 : stats.getMin(),
                                         stats.getMax() == Double.NEGATIVE_INFINITY ? 0 : stats.getMax(),
                                         stats.getCount());
    }
}
