package com.n26.test.controller;

import com.n26.test.model.NamedTransactionStatistics;
import com.n26.test.model.TransactionStatistics;
import com.n26.test.service.statistics.TransactionStatisticsService;
import com.n26.test.service.statistics.TransactionStatisticsType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("statistics")
public class TransactionStatisticsController {
    private final TransactionStatisticsService timeSlotStatistics;
    private final List<TransactionStatisticsService> allStatistics;

    @Autowired
    public TransactionStatisticsController(
            @Qualifier(TransactionStatisticsType.TIME_SLOT) TransactionStatisticsService timeSlotStatistics,
            List<TransactionStatisticsService> allStatistics) {
        this.timeSlotStatistics = timeSlotStatistics;
        this.allStatistics = allStatistics;
    }

    @RequestMapping(method = RequestMethod.GET)
    public TransactionStatistics getLastMinuteApproxStatistics() {
        return timeSlotStatistics.getLastMinuteStatistics();
    }

    @RequestMapping(path = "all", method = RequestMethod.GET)
    public List<NamedTransactionStatistics> getLastMinuteStatistics() {
        List<NamedTransactionStatistics> result = new ArrayList<>();
        for (TransactionStatisticsService service : allStatistics) {
            result.add(new NamedTransactionStatistics(service.getClass().getSimpleName(),
                                                      service.getLastMinuteStatistics()));
        }
        return result;
    }
}
