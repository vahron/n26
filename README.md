# Transaction statistics
## N26 Back-End Java Code Challenge

### Solution main points
* Lock-free
* Java 8
* Gradle
* Unit tests
* Spring Boot, Data
* Clean model/controller/service separation

### Solution
Let's introduce following variables 
```
#!text
N - overall count of transactions during app lifetime
K - average count of transactions in time window of 1 minute
```

#### First approach - use database
First approach to the problem is shown in TransactionStatisticsType.DB service. It's mostly for testing purposes,
because we can rely on DB transaction mechanism. When we've got transactions from DB, make one pass over K elements
to evaluate statistics.

Summary of approach:

* memory: by app - O(1), by DB - O(N)
* cpu: by app - O(K), by DB - O(logN)

#### Second approach - store all transactions that came in one minute window.
Second approach is to maintain data structure with transactions happened in time window. 
When someone acquires statistics, or new transaction arrives, we get rid of old transactions from structure.

My first intention was to implement this based on bunch of PriorityBlockingQueues and Atomics ([file](https://bitbucket.org/vahron/n26/src/81cb14896cc61ef60da18489f27308d30c39b3db/src/main/java/com/n26/test/service/statistics/exact/TransactionStatisticsExactServiceImpl.java?fileviewer=file-view-default)).

But this has two cons:

1. Inconsistent state of statistics. Sum, count, min and max were updated without common lock. That means someone who acquire stat could see half-updated statistics. 
Some fields could be updated with new transaction, some - not
2. PriorityBlockingQueue uses ReentrantLock inside, so it's much or less equal to common lock.
 
So, I ended up with TransactionStatisticsType.EXACT service with common updating lock and PriorityQueue and TreeSet ([file](https://bitbucket.org/vahron/n26/src/7e7ccf7b6d5c0dab98afacb374ef89c642db3efb/src/main/java/com/n26/test/service/statistics/exact/TransactionStatisticsExactServiceImpl.java?at=master&fileviewer=file-view-default)).

When transaction arrives, I add it with cost of O(logK), and usually remove old one with the same cost. 
The worst case is when all K transactions were processed in one moment, and a client acquires stats after 1 minute.
Then we have to perform K delete operations.

Summary of approach:

* memory: O(K) - keep all transactions in time window
* cpu: O(logK) - usually, O(K * logK) - worst case


#### Third approach - split time window into slots
Third approach is to split time window to constant amount of time slots.
```
#!text
S - overall count of timeslots for 1 minute, constant
```
The idea is to provide stats which was actual sometimes in a time slot. Each slot stores it's local sum, count, max and min values. When time passes, I remove old slots and append new ones - for current timestamp. Then when server's got a stat request,
it accumulates all stats from slots. If we have enough slots, then the result would be quite precise. 

My first implementation was based on ConcurrentSkipListMap structure and mutable TimeSlots on Atomics. 
That leads to problem mentioned in first solution - fields of TimeSlot could be partially updated by transaction ([file](https://bitbucket.org/vahron/n26/src/d0c096f641e7ca4e3204453d7597f8815822522c/src/main/java/com/n26/test/service/statistics/timeslot/TransactionStatisticsTimeSlotServiceImpl.java?fileviewer=file-view-default)).

Then I made TimeSlot immutable and store it in ConcurrentHashMap. Each TimeSlot then represents state after a transaction.
When someone wants to get all stats, server passes through all elements in map and evaluates summary. That costs O(S) operations.

The solution guarantees that acquired state and statistics was presented sometime in current time slot. Moreover, all operations are done without common lock.   

To break everything down, we have S time slots, but S is constant and doesn't depend on N and K. Then consumed time and memory are constant O(S) =>O (const) => O(1) in terms of N and K. It's implemented in TransactionStatisticsType.TIME_SLOT ([file](https://bitbucket.org/vahron/n26/src/7e7ccf7b6d5c0dab98afacb374ef89c642db3efb/src/main/java/com/n26/test/service/statistics/timeslot/TransactionStatisticsTimeSlotServiceImpl.java?at=master&fileviewer=file-view-default)).

Summary of approach:

* memory: O(S) - we keep some time slots, O(1) in terms of N
* cpu: O(S) - algorithm sums up statistics of all time slots, O(1) in terms of N

 
#### REST Scheme
```
#!text
POST /transactions/ - create a transaction
GET /transactions/ - fetch all processed transactions

GET /statistics/ - get stats by third approach
GET /statistics/all - get stats by all aproaches
```

#### Examples
##### Create a transaction
```
#!text
POST /transactions/ 
{
	"amount": 1,
	"timestamp": 123
} 
-> 201
```
##### Get all transactions
```
#!text
GET /transactions/ 
-> 200
[
  {
    "amount": 1,
    "timestamp": 123
  },
  {
    "amount": 2,
    "timestamp": 124
  }
]
```
##### Get statistics
```
#!text
GET /statistics/
-> 200
{
  "sum": 2140,
  "avg": 142.3,
  "min": 2,
  "max": 213,
  "count": 15
}
```
##### Get all statistics
```
#!text
GET /statistics/all
-> 200
[
  {
    "sum": 2140,
    "avg": 142.3,
    "min": 213,
    "max": 2,
    "count": 15,
    "provider": "TransactionStatisticsDbBasedServiceImpl"
  },
  {
    "sum": 2140,
    "avg": 142.3,
    "min": 213,
    "max": 2,
    "count": 15,
    "provider": "TransactionStatisticsExactServiceImpl"
  },
  {
    "sum": 2140,
    "avg": 142.3,
    "min": 213,
    "max": 2,
    "count": 15,
    "provider": "TransactionStatisticsTimeSlotServiceImpl"
  }
]
```

### Testing
I provided some unit tests together with solution. They include multithread tests, please check test classes.

### Running
Project uses gradle as build system. 
To run tests, please type 'gradle test'. 
To run the app, please use 'gradle bootRun'